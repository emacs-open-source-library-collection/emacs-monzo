(setq monzo-token (concat "Bearer " (password-store-get "token/monzo")))
(setq monzo-seperator "|")

(defun api/test (url)
  (interactive)
  (request-response-data (request url
                                  :parser 'json-read
                                  :headers `(("Authorization" . ,monzo-token))
                                  :sync t)))

;;(api/test "https://api.monzo.com/accounts")
;;(cdr (assoc-default 'id (assoc-default 'accounts (api/test "https://api.monzo.com/accounts"))))
;;(cdr (assoc-default 'id (cdr (assoc-default 'accounts (api/test "https://api.monzo.com/accounts")))))

(defun api/filter (alist &rest keys)
  (let ((res ()))
    (dolist (x keys)
      (pp (cdr (assoc x alist)))
      (push (cons (cdr (assoc x alist))
                  (car (assoc x alist))) res)) res))

(defun api/headers
    (&rest
     keys)
  (concat "| " (string-join keys " | ") " |"))

(defun api/filter-concat (alist &rest keys)
  (concat "| " (string-join (loop for x in keys collect (cdr (assoc x alist))) " | ") " |"))


(defun api/report ()
  (interactive)
  (with-current-buffer (get-buffer-create "*monzo*")
    (org-mode)
    (insert "#+TITLE: Monzo report")
    (insert api/headers "id" "name")
    (insert api/filter-concat 'id 'name)
    (pp (assoc 'id (elt (assoc-default 'accounts (api/test "https://api.monzo.com/accounts")) 0)))))
